### Jobs Board app

This is a mock Jobs Board application built using Ruby on Rails. 

![image](https://raw.githubusercontent.com/charlesdebarros/Jobs_Board/master/app/assets/images/jobs_board_screenshot_job_form.jpg)

### Technologies used

* Ruby 2.2.1
* Rails 4.2.4
* PostgreSQL 9.3
* Boostrap Sass 3.3.5
* Sass Rails 5.0
* Haml 4.0.7
* Simple Form 3.2.0

This app was developed on a Linux Mint 17.1 (Rebecca) operating system using Sublime Text 1083 and Firefox 41.0